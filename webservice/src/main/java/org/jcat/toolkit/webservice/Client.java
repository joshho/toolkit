package org.jcat.toolkit.webservice;

import org.jcat.toolkit.webservice.client.BusinessService;


public class Client {

	/**
	 * 客户端工程应该单独起一个项目
	 * 
	 * 1、先在命令行调用如下命令，wsimport属行jdk命令，生成相应代码，生成的代码见client包<br/>
	 * wsimport -keep http://localhost:9527/BusinessService?wsdl
	 * 
	 * 2、执行该main方法调用
	 * 
	 * @param args
	 */

	public static void main(String[] args) {
		BusinessService businessService = new BusinessService();
		org.jcat.toolkit.webservice.client.Business business = businessService.getBusinessPort();
		String response = business.echo("hello");
		System.out.println(response);
	}
}
