package org.jcat.toolkit.webservice;

import java.rmi.RemoteException;

import javax.jws.WebService;

@WebService(name = "Business", serviceName = "BusinessService", targetNamespace = "http://demo.jcat.org/webservice/client")
public class BusinessImpl implements Business {

	@Override
	public String echo(String message) throws RemoteException {
		if ("quit".equalsIgnoreCase(message.toString())) {
			System.out.println("server will be shutdown!");
			System.exit(0);
		}
		System.out.println("Message from client: " + message);
		return "Server response: " + message;
	}
}
