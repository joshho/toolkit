package org.jcat.toolkit.springboot;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by hejinxu on 2016/12/16.
 */
public class BaseController {

    @ExceptionHandler
    @ResponseBody
    public String exceptionHandler(HttpServletRequest req, Exception ex) {
        return "exception";
    }

}
