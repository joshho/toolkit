package org.jcat.toolkit.springboot;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by hejinxu on 2016/12/16.
 */
@Controller
public class TestController extends BaseController{

    /**
     * 异常处理器
     * @return
     */
    @ResponseBody
    @RequestMapping("hello")
    public String hello() {
        int i = 3/0;
        return "hello world!";
    }
}
