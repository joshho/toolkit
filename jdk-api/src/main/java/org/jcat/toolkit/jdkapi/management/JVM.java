package org.jcat.toolkit.jdkapi.management;

import java.lang.management.ClassLoadingMXBean;
import java.lang.management.CompilationMXBean;
import java.lang.management.GarbageCollectorMXBean;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.lang.management.ThreadMXBean;
import java.lang.management.RuntimeMXBean;
import java.lang.management.OperatingSystemMXBean;
import java.lang.management.MemoryPoolMXBean;
import java.util.List;

public class JVM {

	public static void main(String[] args) {
		//内容摘自：http://blog.csdn.net/gaojava/article/details/8767597
		
		System.out.println("\nThreadMXBean:");
		// Java 虚拟机线程系统的管理接口 ThreadMXBean
		ThreadMXBean th = (ThreadMXBean) ManagementFactory.getThreadMXBean();
		System.out.println("活动线程的当前数目" + th.getThreadCount());
		System.out.println("返回活动守护线程的当前数目" + th.getDaemonThreadCount());
		System.out.println("Java 虚拟机启动或峰值重置以来峰值活动线程计数" + th.getPeakThreadCount());
		System.out.println("返回当前线程的总 CPU 时间" + th.getCurrentThreadUserTime());
		System.out.println("当前线程在用户模式中执行的 CPU 时间" + th.getCurrentThreadUserTime());

		
		
		System.out.println("\nRuntimeMXBean:");
		// Java 虚拟机的运行时系统的管理接口。 RuntimeMXBean
		RuntimeMXBean run = ManagementFactory.getRuntimeMXBean();
		System.out.println("正在运行的 Java 虚拟机的名称" + run.getName());
		System.out.println("Java 虚拟机规范名称" + run.getSpecName());
		System.out.println("返回 Java 库路径" + run.getLibraryPath());
		System.out.println("系统类加载器用于搜索类文件的 Java 类路径" + run.getClassPath());
		System.out.println(run.getStartTime());
		System.out.println(run.getUptime());
		System.out.println(run.getLibraryPath());
		System.out.println(run.getManagementSpecVersion());
		System.out.println(run.getVmName());
		System.out.println(run.getVmVendor());
		System.out.println(run.getSpecVendor());
		System.out.println(run.getSpecVersion());
		System.out.println(run.getSystemProperties());
		
		
		
		
		System.out.println("\nOperatingSystemMXBean:");
		// 用于操作系统的管理接口，Java 虚拟机在此操作系统上运行 OperatingSystemMXBean
		OperatingSystemMXBean op = ManagementFactory.getOperatingSystemMXBean();
		System.out.println("操作系统的架构" + op.getArch());
		System.out.println("操作系统名称" + op.getName());
		System.out.println("操作系统的版本" + op.getVersion());
		System.out.println("Java 虚拟机可以使用的处理器数目" + op.getAvailableProcessors());
		System.out.println("getSystemLoadAverage: " + op.getSystemLoadAverage());

		
		System.out.println("\ncom.sun.management.OperatingSystemMXBean:");
		com.sun.management.OperatingSystemMXBean op2 = (com.sun.management.OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean();
		System.out.println(op2.getArch());
		System.out.println(op2.getAvailableProcessors());
		System.out.println(op2.getCommittedVirtualMemorySize());
		System.out.println(op2.getFreePhysicalMemorySize());
		System.out.println(op2.getFreeSwapSpaceSize());
		System.out.println(op2.getName());
		System.out.println(op2.getProcessCpuLoad());
		System.out.println(op2.getSystemLoadAverage());
		System.out.println(op2.getTotalPhysicalMemorySize());
		System.out.println(op2.getTotalSwapSpaceSize());
		System.out.println(op2.getVersion());
		
		
		// MemoryPoolMXBean
		System.out.println("\nMemoryPoolMXBean:");
		// 内存池的管理接口。内存池表示由 Java 虚拟机管理的内存资源，
		// 由一个或多个内存管理器对内存池进行管理 MemoryPoolMXBean
		List<MemoryPoolMXBean> list = ManagementFactory.getMemoryPoolMXBeans();
		for (MemoryPoolMXBean mem : list) {
			System.out.println("Java 虚拟机启动以来或自峰值重置以来此内存池的峰值内存使用量" + mem.getPeakUsage());
			System.out.println("返回此内存池的类型" + mem.getType());
			System.out.println("内存使用量超过其阈值的次数" + mem.getUsage());
		}

		// MemoryMXBean
		System.out.println("\nMemoryMXBean:");
		// Java 虚拟机内存系统的管理接口。 MemoryMXBean
		MemoryMXBean mem = (MemoryMXBean) ManagementFactory.getMemoryMXBean();
		System.out.println("用于对象分配的堆的当前内存使用量" + mem.getHeapMemoryUsage());
		System.out.println("Java 虚拟机使用的非堆内存的当前内存使用量" + mem.getNonHeapMemoryUsage());

		// CompilationMXBean
		System.out.println("\nCompilationMXBean:");
		// Java 虚拟机的编译系统的管理接口 CompilationMXBean
		CompilationMXBean com = (CompilationMXBean) ManagementFactory.getCompilationMXBean();
		System.out.println("即时 (JIT) 编译器的名称" + com.getName());
		System.out.println("在编译上花费的累积耗费时间的近似值(毫秒)" + com.getTotalCompilationTime());

		System.out.println("\nClassLoadingMXBean:");
		// Java 虚拟机的类加载系统的管理接口 ClassLoadingMXBean
		ClassLoadingMXBean cl = (ClassLoadingMXBean) ManagementFactory.getClassLoadingMXBean();
		System.out.println("当前加载到 Java 虚拟机中的类的数量" + cl.getLoadedClassCount());
		System.out.println("Java 虚拟机开始执行到目前已经加载的类的总数" + cl.getTotalLoadedClassCount());
		System.out.println("Java 虚拟机开始执行到目前已经卸载的类的总数" + cl.getUnloadedClassCount());

		System.out.println("\nGarbageCollectorMXBean:");
		List<GarbageCollectorMXBean> gars = ManagementFactory.getGarbageCollectorMXBeans();
		for (GarbageCollectorMXBean gar : gars) {
			System.out.println(gar.getName() + "垃圾回收总数量：" + gar.getCollectionCount());
			System.out.println(gar.getName() + "垃圾回收总时间：" + gar.getCollectionTime());
			System.out.println(gar.getObjectName());
			String[] memPools = gar.getMemoryPoolNames();
			for (String s : memPools) {
				System.out.println("memoryPoolName: " + s);
			}

		}

	}

}