package org.jcat.toolkit.jdkapi.eventobject;

public class Main {
  
    public static void main(String[] args) {  
        MyObject producer = new MyObject();
        producer.addListener(new MyValueChangeListener());
        producer.setValue(2);  
  
    }  
  
}  