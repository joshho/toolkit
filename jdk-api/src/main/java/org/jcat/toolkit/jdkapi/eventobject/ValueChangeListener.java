package org.jcat.toolkit.jdkapi.eventobject;

public interface ValueChangeListener extends java.util.EventListener {
  
    public abstract void performed(MyValueChangeEvent e);
}  