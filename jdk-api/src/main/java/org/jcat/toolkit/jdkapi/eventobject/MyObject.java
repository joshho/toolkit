package org.jcat.toolkit.jdkapi.eventobject;

public class MyObject {
    ListenerRegister register = new ListenerRegister();  
    private int value;  
  
    public int getValue() {  
        return value;  
    }  
  
    public void setValue(int newValue) {  
        if (value != newValue) {  
            value = newValue;  
            MyValueChangeEvent event = new MyValueChangeEvent(this, value);
            fireAEvent(event);  
        }  
    }  
  
    public void addListener(ValueChangeListener a) {  
        register.addListener(a);  
    }  
  
    public void removeListener(ValueChangeListener a) {  
        register.removeListener(a);  
    }  
  
    public void fireAEvent(MyValueChangeEvent event) {
        register.fireAEvent(event);  
    }  
  
}  