package org.jcat.toolkit.jdkapi.eventobject;

import java.util.EventObject;
  
public class MyValueChangeEvent extends EventObject {
  
    /** 
     *  
     */  
    private static final long serialVersionUID = 767352958358520268L;  
    private int value;  
  
    public MyValueChangeEvent(Object source) {
        this(source, 0);  
    }  
  
    public MyValueChangeEvent(Object source, int newValue) {
        super(source);  
        value = newValue;  
    }  
  
    public int getValue() {  
        return value;  
    }  
}  