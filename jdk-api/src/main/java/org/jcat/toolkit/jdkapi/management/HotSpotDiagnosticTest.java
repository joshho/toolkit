package org.jcat.toolkit.jdkapi.management;

import java.io.File;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.util.Properties;

import javax.management.MBeanServer;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

import com.sun.management.HotSpotDiagnosticMXBean;

import sun.management.HotSpotDiagnostic;

public class HotSpotDiagnosticTest {
	public static void main(String[] args) throws IOException {
		HotSpotDiagnosticMXBean b = new HotSpotDiagnostic();

		// 监控应用与被监控应用位于同一JVM
		MBeanServer server = ManagementFactory.getPlatformMBeanServer();
		RuntimeMXBean rmxb = ManagementFactory.newPlatformMXBeanProxy(server, "java.lang:type=Runtime", RuntimeMXBean.class);

		// 2.监控应用与被监控应用不位于同一JVM
		/*
		 * 1)首先在被监控的JVM的启动参数中加入如下的启动参数以启JVM代理 -Dcom.sun.management.jmxremote
		 * -Dcom.sun.management.jmxremote.port=127.0.0.1:8000
		 * -Dcom.sun.management.jmxremote.authenticate=false
		 * -Dcom.sun.management.jmxremote.ssl=false 2)连接到代理上
		 */
		JMXServiceURL url = new JMXServiceURL("service:jmx:rmi:///jndi/rmi://127.0.0.1:8000/jmxrmi");
		JMXConnector connector = JMXConnectorFactory.connect(url);
		RuntimeMXBean rmxb2 = ManagementFactory.newPlatformMXBeanProxy(connector.getMBeanServerConnection(), "java.lang:type=Runtime", RuntimeMXBean.class);

		// 3.监控应用与被监控应用不位于同一JVM但在同一物理主机上(2的特化情况，通过进程Attach)
		
	}

}
