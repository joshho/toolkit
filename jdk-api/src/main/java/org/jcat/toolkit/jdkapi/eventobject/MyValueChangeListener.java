package org.jcat.toolkit.jdkapi.eventobject;

public class MyValueChangeListener implements ValueChangeListener {

    @Override
    public void performed(MyValueChangeEvent e) {
        System.out.println("value changed, new value = " + e.getValue());
    }
}  