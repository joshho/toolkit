package org.jcat.toolkit.pointboundary;
import java.util.ArrayList;
import java.util.List;


public class Data {
	
	
	public static List<Double[]> pointList = new ArrayList<Double[]>();
	
	
	static {
		pointList.add(new Double[]{116.307689, 40.056974});
		pointList.add(new Double[]{116.317502, 40.044195});
		pointList.add(new Double[]{116.319593, 40.045119});
		pointList.add(new Double[]{116.314645, 40.042268});
		pointList.add(new Double[]{116.315038, 40.045461});
		pointList.add(new Double[]{116.313066, 40.045703});
		pointList.add(new Double[]{116.309217, 40.059312});

	}
	
	
}
