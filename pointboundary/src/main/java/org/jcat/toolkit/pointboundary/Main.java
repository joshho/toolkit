package org.jcat.toolkit.pointboundary;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {

	
	public static void main(String[] args) {
		List<Double[]> pointList =  Data.pointList;
		
		List<Point> pList = new ArrayList<Point>();
		for (Double[] xy : pointList) {
			pList.add(new Point(xy[0], xy[1]));
		}
		
		List<Point> boundaryList = MinimumBoundingPolygon.findSmallestPolygon(pList);
		
		for (Point point : boundaryList) {
			System.out.println(point.getX() + "," +  point.getY());
		}
		
	}
	
	
	
	/*public static void main111(String[] args) {
		List<Double[]> pointList =  Data.pointList;
		
		//
		List<Double> longitudeList = ArrayUtil.getColumn(pointList, 1);
		Collections.sort(longitudeList);
		
		
		
		for (Double double1 : longitudeList) {
			System.out.println(double1);
		}
	}*/
	
	
	
	
	
	
}
