package org.jcat.toolkit.pointboundary;

import java.util.ArrayList;
import java.util.List;

public class ArrayUtil {

	public static <T> List<T> getColumn(List<T[]> arrayList, int index) {
		List<T> result = new ArrayList<T>();
		for (int i = 0; i < arrayList.size(); i++) {
			T[] o = arrayList.get(i);
			result.add(o[index]);
		}
		return result;
	}
	
}
