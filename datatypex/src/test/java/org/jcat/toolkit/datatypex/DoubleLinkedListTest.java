package org.jcat.toolkit.datatypex;

import org.jcat.toolkit.datatypex.DoubleLinkedList.Node;

public class DoubleLinkedListTest {
	public static void main(String[] args) {
        DoubleLinkedList list = new DoubleLinkedList();
        for (int i = 0; i < 10; i++) {
            list.insert(i, i);
        }
        System.out.println("原来的链表为:");
        list.printList();
        System.out.println("链表的长度为:" + list.size());
        System.out.println();

        //deleteByIndex测试
        list.deleteByIndex(5);
        System.out.println("删除第6个元素后的链表为:");
        list.printList();

        System.out.println();
        System.out.println("删除元素后的链表的长度为:" + list.size());

        //getNodeVal测试
        Integer idem = (Integer) list.getNodeVal(5);
        System.out.println("删除第6个元素后，第6个元素的值为：" + idem);
        System.out.println();

        //insert(node)测试
        list.insert(20);
        System.out.println("插入1个20到最后，链表数据为：");
        list.printList();
        System.out.println();

        //insert(node)测试
        list.insert(11);
        System.out.println("插入1个11到最后，链表数据为：");
        list.printList();
        System.out.println();

        //insert(i, node)测试
        list.insert(0, 12);
        System.out.println("在头节点插入1个12，链表数据为：");
        list.printList();
        System.out.println();

        //getNode(i)测试
        Node n = list.getNode(0);
        System.out.println("第0个节点的元素值为：" + n.value);
        System.out.println("第0个节点的下一个元素的值为：" + n.next.value);
        System.out.println("第0个节点的上一个元素的值为：" + n.prev.value);
        System.out.println();


        //insert(i, node)测试
        list.insert(0, 13);
        System.out.println("在头节点插入13之后的链表数据为：");
        list.printList();
        System.out.println();

        //getNode测试
        Node n2 = list.getNode(0);
        System.out.println("第0个节点的元素值为：" + n2.value);
        System.out.println("第0个节点的下一个元素的值为：" + n2.next.value);
        System.out.println("第0个节点的上一个元素的值为：" + n2.prev.value);
        System.out.println();

        //getNode测试
        Node n3 = list.getNode(7);
        System.out.println("第8个节点的元素值为：" + n3.value);
        System.out.println("第8个节点的下一个元素的值为：" + n3.next.value);
        System.out.println("第8个节点的上一个元素的值为：" + n3.prev.value);
        System.out.println();

        //getNodeVal测试
        Integer idem2 = (Integer) list.getNodeVal(3);
        System.out.println("第4个元素的值为：" + idem2);
        System.out.println();

        //deleteByIndex测试
        list.deleteByIndex(0);
        System.out.println("删除头节点后的链表为:");
        list.printList();

        //deleteByIndex测试
        list.deleteByIndex(0);
        System.out.println("删除头节点后的链表为:");
        list.printList();

    }
}
