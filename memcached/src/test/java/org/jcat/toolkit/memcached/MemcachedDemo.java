package org.jcat.toolkit.memcached;

import java.io.Serializable;

import com.whalin.MemCached.MemCachedClient;

public class MemcachedDemo {

	public static void main(String[] args) {
		MemcachedPool.initialize();
		
		/**
		 * 创建一个memcached客户端，所有对memcached中数据操作的方法都在这个类里面
		 */
		MemCachedClient memCachedClient = new MemCachedClient();
		/**
		 * 存储一个username，值为刘德华，存储成功返回true
		 */
		boolean success = memCachedClient.set("username", "刘德华");
		System.out.println(success);

		/**
		 * 从缓存中获取一个key为username的数据
		 */
		Object o = memCachedClient.get("username");
		System.out.println(o);

		/**
		 * 定义一个p对象，Person类必须实现Serializable接口
		 */
		Person p = new Person();
		p.setId("1");
		p.setName("周杰伦");

		/**
		 * 缓存一个p对象
		 */
		memCachedClient.set("p1", p);

		/**
		 * 获取缓存的p对象
		 */
		Person p1 = (Person) memCachedClient.get("p1");
		System.out.println(p1.getName());

		/**
		 * 调用add方法添加一个key为p1的对象，值123是不能添加进缓存的，因为p1已经添加过一次了
		 */
		memCachedClient.add("p1", 123);// 错误！无法更新p1的值

		System.out.println(memCachedClient.get("p1"));// 还是person对象

		/**
		 * 使用set方法可以更新p1
		 */
		memCachedClient.set("p1", 123);
		System.out.println(memCachedClient.get("p1"));// 输出123

		/**
		 * 使用replace方法可以更新p1
		 */
		memCachedClient.replace("p1", 456);
		System.out.println(memCachedClient.get("p1"));// 输出456

		/**
		 * 使用replace方法可以更新p2,缓存中不含有key为p2的数据，无法更新，不会添加
		 */
		memCachedClient.replace("p2", 456);
		System.out.println(memCachedClient.get("p2"));// 输出null

		/**
		 * 删除key为p1的缓存数据
		 */
		memCachedClient.delete("p1");
		System.out.println(memCachedClient.get("p1"));// 输出null
	}
}

class Person implements Serializable {

	private static final long serialVersionUID = 1L;

	private String id;
	private String name;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}