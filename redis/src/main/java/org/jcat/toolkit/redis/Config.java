package org.jcat.toolkit.redis;

import java.io.IOException;

import org.jcat.toolkit.utils.PropertiesUtil;

public class Config {
	
	private static String filePath = "redis.properties";
	private static final PropertiesUtil util = new PropertiesUtil(filePath);
			
	public static String getString(String key) {
		try {
			return util.readValue(key);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static Integer getInt(String key) {
		try {
			return Integer.valueOf(util.readValue(key));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

}
