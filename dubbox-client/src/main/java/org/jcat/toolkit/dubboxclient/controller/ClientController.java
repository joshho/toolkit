package org.jcat.toolkit.dubboxclient.controller;

import org.jcat.toolkit.dubbox.bean.User;
import org.jcat.toolkit.dubbox.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by joshho on 17/5/10.
 */
@Controller
public class ClientController {

    @Autowired
    LoginService loginService;

    @RequestMapping
    @ResponseBody
    public String test(){

        User u = new User();
        u.setUsername("张三");
        boolean b = loginService.login(u);

        return "server return: " + b;
    }

}
