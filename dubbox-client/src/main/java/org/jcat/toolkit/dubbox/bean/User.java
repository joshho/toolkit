package org.jcat.toolkit.dubbox.bean;

import java.io.Serializable;

/**
 * Created by joshho on 17/5/10.
 */
public class User implements Serializable{

    String id;
    String username;
    String password;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
