package org.jcat.toolkit.rmi;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Client {
	public static void main(String[] args) throws RemoteException, NotBoundException {
		Registry registry = LocateRegistry.getRegistry("localhost");
		String name = "BusinessDemo";
		Business business = (Business) registry.lookup(name);
		System.out.println(business.echo("hello"));
	}
}
