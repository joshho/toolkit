package org.jcat.toolkit.rmi;

import java.rmi.RemoteException;

public class BusinessImpl implements Business {

	@Override
	public String echo(String message) throws RemoteException {
		if ("quit".equalsIgnoreCase(message.toString())) {
			System.out.println("server will be shutdown!");
			System.exit(0);
		}
		System.out.println("Message from client: " + message);
		return "Server response: " + message;
	}
}
