package org.jcat.toolkit.designpattern.cate1.factoryMethod.type2;

public interface Sender {
	public void Send();
}