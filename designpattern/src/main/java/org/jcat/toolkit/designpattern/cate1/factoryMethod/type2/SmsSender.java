package org.jcat.toolkit.designpattern.cate1.factoryMethod.type2;
public class SmsSender implements Sender {  
  
    @Override  
    public void Send() {  
		System.out.println("this is sms sender!");
    }  
}  