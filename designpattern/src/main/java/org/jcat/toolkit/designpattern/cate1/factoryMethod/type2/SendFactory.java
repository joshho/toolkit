package org.jcat.toolkit.designpattern.cate1.factoryMethod.type2;

public class SendFactory {
	public Sender produceMail() {
		return new MailSender();
	}

	public Sender produceSms() {
		return new SmsSender();
	}
}