package org.jcat.toolkit.designpattern.cate1.abstractFactory;
public class MailSender implements Sender {  
    @Override  
    public void Send() {  
		System.out.println("this is mailsender!");
    }  
}  