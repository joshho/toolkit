package org.jcat.toolkit.designpattern.cate2.proxy;
public interface Sourceable {  
    public void method();  
}  