package org.jcat.toolkit.designpattern.cate3.interpreter.sample2;

public class TerminalExpression extends AbstractExpression
{
    private final int i;

    public TerminalExpression(final int i)
    {
        this.i = i;
    }

    @Override
    public int interpreter(final Context context)
    {
        return this.i;
    }

}