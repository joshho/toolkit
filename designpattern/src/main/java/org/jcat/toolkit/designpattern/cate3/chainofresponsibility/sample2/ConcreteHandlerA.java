package org.jcat.toolkit.designpattern.cate3.chainofresponsibility.sample2;

/**
 * 具体职责处理者A：案例中的 部门组长角色
 * @author  lvzb.software@qq.com
 *
 */
public class ConcreteHandlerA extends Handler {

	@Override
    public void handleRequest(int leaveDay) {
		if(leaveDay <= 1){
			// 满足处理条件  处理请求
			System.out.println("请假天数小于2天  由部门组长审批");
		} else {
			nextHandler.handleRequest(leaveDay);
		}

	}

}