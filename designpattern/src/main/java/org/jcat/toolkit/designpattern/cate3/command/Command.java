package org.jcat.toolkit.designpattern.cate3.command;

public interface Command {
	public void exe();
}