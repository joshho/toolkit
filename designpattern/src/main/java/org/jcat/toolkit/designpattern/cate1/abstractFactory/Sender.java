package org.jcat.toolkit.designpattern.cate1.abstractFactory;

public interface Sender {
	public void Send();
}