package org.jcat.toolkit.designpattern.cate3.strategy;

public interface ICalculator {
	public int calculate(String exp);
}