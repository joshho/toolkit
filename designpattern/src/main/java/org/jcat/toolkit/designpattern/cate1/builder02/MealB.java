package org.jcat.toolkit.designpattern.cate1.builder02;

public class MealB extends MealBuilder{
  
    public void buildDrink() {  
        meal.setDrink("一杯柠檬果汁");  
    }  
  
    public void buildFood() {  
        meal.setFood("三个鸡翅");  
    }  
  
}  