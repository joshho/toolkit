package org.jcat.toolkit.designpattern.cate1.builder02;

public abstract class MealBuilder {
    Meal meal = new Meal();  
      
    public abstract void buildFood();  
      
    public abstract void buildDrink();  
      
    public Meal getMeal(){  
        return meal;  
    }  
}  