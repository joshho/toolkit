package org.jcat.toolkit.designpattern.cate2.bridge;

public abstract class Log {
    public ImpLog implementor;
      
    public  void write(String log)
    {
        implementor.execute(log);
    }
}