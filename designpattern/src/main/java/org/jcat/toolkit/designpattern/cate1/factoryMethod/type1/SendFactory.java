package org.jcat.toolkit.designpattern.cate1.factoryMethod.type1;
public class SendFactory {  
  
    public Sender produce(String type) {  
        if ("mail".equals(type)) {  
            return new MailSender();  
		} else if ("sms".equals(type)) {
            return new SmsSender();  
        } else {  
			System.out.println("请输入正确的类型!");
            return null;  
        }  
    }  
}  