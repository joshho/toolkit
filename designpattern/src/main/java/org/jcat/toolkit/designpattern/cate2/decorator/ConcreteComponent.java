package org.jcat.toolkit.designpattern.cate2.decorator;

public class ConcreteComponent implements Component {

    @Override
    public void sampleOperation() {
        // 写相关的业务代码
    }

}