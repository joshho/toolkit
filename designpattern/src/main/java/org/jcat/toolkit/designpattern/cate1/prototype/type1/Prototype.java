package org.jcat.toolkit.designpattern.cate1.prototype.type1;
public class Prototype implements Cloneable {  
  
    @Override
	public Object clone() throws CloneNotSupportedException {  
        Prototype proto = (Prototype) super.clone();  
        return proto;  
    }  
}  