package org.jcat.toolkit.designpattern.cate1.factoryMethod.type3;

public interface Sender {
	public void Send();
}