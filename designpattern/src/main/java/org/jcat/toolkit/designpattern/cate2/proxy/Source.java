package org.jcat.toolkit.designpattern.cate2.proxy;
public class Source implements Sourceable {  
  
    @Override  
    public void method() {  
		System.out.println("the original method!");
    }  
}  