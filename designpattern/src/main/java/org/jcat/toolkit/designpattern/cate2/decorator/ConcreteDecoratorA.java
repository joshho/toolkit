package org.jcat.toolkit.designpattern.cate2.decorator;

public class ConcreteDecoratorA extends Decorator {

    public ConcreteDecoratorA(Component component) {
        super(component);
    }
    
    @Override
    public void sampleOperation() {
        super.sampleOperation();
        // 写相关的业务代码
    }
}