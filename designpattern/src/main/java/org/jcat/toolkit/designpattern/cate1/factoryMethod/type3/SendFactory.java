package org.jcat.toolkit.designpattern.cate1.factoryMethod.type3;
public class SendFactory {  
      
    public static Sender produceMail(){  
        return new MailSender();  
    }  
      
    public static Sender produceSms(){  
        return new SmsSender();  
    }  
}  