package org.jcat.toolkit.designpattern.cate2.bridge;

public abstract class ImpLog {
     public abstract void execute(String msg);
}