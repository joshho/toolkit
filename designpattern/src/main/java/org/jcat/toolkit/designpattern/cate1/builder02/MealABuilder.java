package org.jcat.toolkit.designpattern.cate1.builder02;

public class MealABuilder extends MealBuilder{
  
    public void buildDrink() {  
        meal.setDrink("一杯可乐");  
    }  
  
    public void buildFood() {  
        meal.setFood("一盒薯条");  
    }  
  
}  