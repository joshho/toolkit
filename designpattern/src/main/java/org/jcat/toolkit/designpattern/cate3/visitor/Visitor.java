package org.jcat.toolkit.designpattern.cate3.visitor;
public interface Visitor {  
    public void visit(Subject sub);  
}  