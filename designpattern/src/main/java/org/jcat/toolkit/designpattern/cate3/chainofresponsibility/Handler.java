package org.jcat.toolkit.designpattern.cate3.chainofresponsibility;

public interface Handler {
	public void operator();
}