package org.jcat.toolkit.designpattern.cate3.chainofresponsibility.sample2;

/**
 * 客户端类<br/>
 * 负责创建职责链和发送请求<br/>
 * 当职责链（职责的传递顺序/请求的处理顺序）建好之后，客户端只负责将请求发送出去，
 * 而具体请求在职责链上的传递和最终由链上的哪个对象进行处理不由客户端关心
 * @author  lvzb.software@qq.com
 *
 */
public class Client {

	public static void main(String[] args) {
        Handler handlerA,handlerB,handlerC;
		handlerA = new ConcreteHandlerA();
		handlerB = new ConcreteHandlerB();
		handlerC = new ConcreteHandlerC();
		
		// 创建职责链  handlerA ——> handlerB ——> handlerC
		handlerA.setNextHandler(handlerB);
		handlerB.setNextHandler(handlerC);
		
		// 发送请假请求一
		handlerA.handleRequest(1);
		
		// 发送请假请求二
		handlerA.handleRequest(7);
		
		// 发送请假请求二
		handlerA.handleRequest(3); 
	}

}