package org.jcat.toolkit.designpattern.cate2.bridge;

public class Client {
 
    public static void main(String[] args) {
        Log dblog = new DatabaseLog();
        dblog.implementor = new NImpLog();
        dblog.write("NET平台下的Database Log");
 
        Log txtlog = new TextFileLog();
        txtlog.implementor = new JImpLog();
        txtlog.write("Java平台下的Text File Log");
    }
 
}