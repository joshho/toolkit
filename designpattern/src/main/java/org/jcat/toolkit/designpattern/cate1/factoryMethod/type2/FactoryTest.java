package org.jcat.toolkit.designpattern.cate1.factoryMethod.type2;
public class FactoryTest {  
  
    public static void main(String[] args) {  
        SendFactory factory = new SendFactory();  
		Sender sender = factory.produceMail();
        sender.Send();  
    }  
}  