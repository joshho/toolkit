package org.jcat.toolkit.designpattern.cate3.chainofresponsibility.sample2;

/**
 * 抽象处理者类
 * @author  lvzb.software@qq.com
 *
 */
public abstract class Handler {

    protected Handler nextHandler;
	
	public void setNextHandler(Handler nextHandler){
		this.nextHandler = nextHandler;
	}
	
	public abstract void handleRequest(int request);

}