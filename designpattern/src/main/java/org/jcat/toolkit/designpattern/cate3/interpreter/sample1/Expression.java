package org.jcat.toolkit.designpattern.cate3.interpreter.sample1;

import org.jcat.toolkit.designpattern.cate3.interpreter.sample1.Context;

public interface Expression {
	public int interpret(Context context);
}