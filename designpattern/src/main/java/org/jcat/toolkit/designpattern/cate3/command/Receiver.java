package org.jcat.toolkit.designpattern.cate3.command;

public class Receiver {
	public void action() {
		System.out.println("command received!");
	}
}