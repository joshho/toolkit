package org.jcat.toolkit.designpattern.cate2.decorator;

public interface Component {
    
    public void sampleOperation();
    
}