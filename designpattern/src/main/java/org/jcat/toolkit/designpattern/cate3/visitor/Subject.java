package org.jcat.toolkit.designpattern.cate3.visitor;
public interface Subject {  
    public void accept(Visitor visitor);  
    public String getSubject();  
}  