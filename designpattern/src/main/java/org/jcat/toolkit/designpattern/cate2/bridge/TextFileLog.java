package org.jcat.toolkit.designpattern.cate2.bridge;

public class TextFileLog extends Log{
    public void write(String log) {
        implementor.execute(log);
    }
}