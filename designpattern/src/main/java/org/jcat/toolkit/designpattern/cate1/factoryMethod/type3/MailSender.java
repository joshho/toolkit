package org.jcat.toolkit.designpattern.cate1.factoryMethod.type3;

public class MailSender implements Sender {
	@Override
	public void Send() {
		System.out.println("this is mailsender!");
	}
}