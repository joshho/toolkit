package org.jcat.toolkit.designpattern.cate3.chainofresponsibility.sample2;

/**
 * 具体职责处理者B：案例中的 部门经理角色
 * @author  lvzb.software@qq.com
 *
 */
public class ConcreteHandlerB extends Handler {

	@Override
    public void handleRequest(int leaveDay) {
		if(1 < leaveDay && leaveDay <= 5){
			// 满足处理条件  处理请求
			System.out.println("请假天数大于1天且小于等于5天  由部门经理审批");
		} else {
			nextHandler.handleRequest(leaveDay);
		}

	}

}