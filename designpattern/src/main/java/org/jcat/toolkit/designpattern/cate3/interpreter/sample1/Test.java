package org.jcat.toolkit.designpattern.cate3.interpreter.sample1;

import org.jcat.toolkit.designpattern.cate3.interpreter.sample1.Context;
import org.jcat.toolkit.designpattern.cate3.interpreter.sample1.Minus;
import org.jcat.toolkit.designpattern.cate3.interpreter.sample1.Plus;

public class Test {

	public static void main(String[] args) {

		// 计算9+2-8的值
		int result = new Minus().interpret(new Context(new Plus().interpret(new Context(9, 2)), 8));
		System.out.println(result);
	}
}