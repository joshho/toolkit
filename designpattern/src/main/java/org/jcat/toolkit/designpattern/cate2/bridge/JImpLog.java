package org.jcat.toolkit.designpattern.cate2.bridge;

public class JImpLog extends ImpLog {
    public void execute(String msg) {
        System.out.println(msg);
    }
}