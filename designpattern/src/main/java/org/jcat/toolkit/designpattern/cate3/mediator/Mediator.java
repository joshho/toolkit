package org.jcat.toolkit.designpattern.cate3.mediator;

public interface Mediator {
	public void createMediator();

	public void workAll();
}