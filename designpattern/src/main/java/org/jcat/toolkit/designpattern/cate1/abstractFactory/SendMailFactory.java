package org.jcat.toolkit.designpattern.cate1.abstractFactory;


public class SendMailFactory implements Provider {  
      
    @Override  
    public Sender produce(){  
        return new MailSender();  
    }  
}  