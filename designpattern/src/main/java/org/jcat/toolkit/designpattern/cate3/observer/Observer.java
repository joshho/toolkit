package org.jcat.toolkit.designpattern.cate3.observer;

public interface Observer {
	public void update();
}