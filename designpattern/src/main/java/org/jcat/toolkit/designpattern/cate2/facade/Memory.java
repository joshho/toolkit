package org.jcat.toolkit.designpattern.cate2.facade;

public class Memory {

	public void startup() {
		System.out.println("memory startup!");
	}

	public void shutdown() {
		System.out.println("memory shutdown!");
	}
}