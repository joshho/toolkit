package org.jcat.toolkit.designpattern.cate1.factoryMethod.type1;

public interface Sender {
	public void Send();
}