package org.jcat.toolkit.designpattern.cate3.visitor;
public class MySubject implements Subject {  
  
    @Override  
    public void accept(Visitor visitor) {  
        visitor.visit(this);  
    }  
  
    @Override  
    public String getSubject() {  
		return "love";
    }  
}  