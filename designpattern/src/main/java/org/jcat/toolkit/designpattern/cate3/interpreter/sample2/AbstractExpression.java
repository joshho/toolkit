package org.jcat.toolkit.designpattern.cate3.interpreter.sample2;

public abstract class AbstractExpression
{
    public abstract int interpreter(Context context);
}