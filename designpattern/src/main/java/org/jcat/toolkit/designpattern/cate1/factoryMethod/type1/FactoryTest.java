package org.jcat.toolkit.designpattern.cate1.factoryMethod.type1;
public class FactoryTest {  
  
    public static void main(String[] args) {  
        SendFactory factory = new SendFactory();  
		Sender sender = factory.produce("sms");
        sender.Send();  
    }  
}  