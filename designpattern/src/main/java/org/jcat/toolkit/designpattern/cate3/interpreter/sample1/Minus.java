package org.jcat.toolkit.designpattern.cate3.interpreter.sample1;

import org.jcat.toolkit.designpattern.cate3.interpreter.sample1.Context;
import org.jcat.toolkit.designpattern.cate3.interpreter.sample1.Expression;

public class Minus implements Expression {

	@Override
	public int interpret(Context context) {
		return context.getNum1() - context.getNum2();
	}
}