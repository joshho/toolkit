package org.jcat.toolkit.designpattern.cate1.factoryMethod.type3;
public class FactoryTest {  
  
    public static void main(String[] args) {      
        Sender sender = SendFactory.produceMail();  
        sender.Send();  
    }  
}  