package org.jcat.toolkit.designpattern.cate1.abstractFactory;

public interface Provider {
	public Sender produce();
}