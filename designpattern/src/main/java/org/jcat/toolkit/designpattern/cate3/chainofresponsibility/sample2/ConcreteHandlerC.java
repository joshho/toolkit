package org.jcat.toolkit.designpattern.cate3.chainofresponsibility.sample2;

/**
 * 具体职责处理者C：案例中的 总经理角色
 * @author  lvzb.software@qq.com
 *
 */
public class ConcreteHandlerC extends Handler {

	@Override
    public void handleRequest(int leaveDay) {
		if(leaveDay > 5){
			System.out.println("当请假天数大于5天的情况下 由总经理亲自操刀审批。总经理的职责已经是最大的啦，还有他没有权限处理的事吗？");
		}
	}

}