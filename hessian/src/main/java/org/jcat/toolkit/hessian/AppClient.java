package org.jcat.toolkit.hessian;

import com.caucho.hessian.client.HessianProxyFactory;

public class AppClient {
	public static void main(String[] args) throws Exception {
		HessianProxyFactory factory = new HessianProxyFactory();

		String url = "http://127.0.0.1:8080/hessian/user";

		UserService us = (UserService) factory.create(UserService.class, url);

		System.out.println(us.getById(2l));
		System.out.println(us.get());
	}
}
