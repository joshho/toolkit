package org.jcat.toolkit.hessian;

import java.util.HashMap;
import java.util.Map;

public class UserServiceImpl implements UserService {
	public String get() {
		return "get method invoke";
	}

	public Map<String, Object> getById(long id) {
		if (id <= 0) {
			return null;
		}
		Map<String, Object> result = new HashMap<>();
		result.put("id", id);
		result.put("name", "英布");
		result.put("position", "九江王");
		return result;
	}
}