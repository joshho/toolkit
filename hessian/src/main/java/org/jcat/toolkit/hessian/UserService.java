package org.jcat.toolkit.hessian;

import java.util.Map;

public interface UserService {

	public Map<String, Object> getById(long id);

	public String get();
}