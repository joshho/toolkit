package org.jcat.toolkit.dubbox.service;

import org.jcat.toolkit.dubbox.bean.User;
import org.springframework.stereotype.Service;

/**
 * Created by joshho on 17/5/10.
 */
public interface LoginService {

    public boolean login(User user);
}
