package org.jcat.toolkit.dubbox.service.impl;

import org.apache.log4j.Logger;
import org.jcat.toolkit.dubbox.bean.User;
import org.jcat.toolkit.dubbox.service.LoginService;
import org.springframework.stereotype.Service;


/**
 * Created by joshho on 17/5/10.
 */
@Service
public class LoginServiceImpl implements LoginService{

    private static final Logger LOGGER = Logger.getLogger(LoginServiceImpl.class);

    @Override
    public boolean login(User user) {

        LOGGER.info("用户:" + user.getUsername() + "登录成功");

        return true;
    }
}
