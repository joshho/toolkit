package org.jcat.toolkit.dubbox.controller;

import org.jcat.toolkit.dubbox.bean.User;
import org.jcat.toolkit.dubbox.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by joshho on 17/5/10.
 */
@Controller
public class LoginController {

    @Autowired
    LoginService loginService;

    @RequestMapping("login")
    @ResponseBody
    public boolean login() {
        User u = new User();
        loginService.login(u);
        return true;
    }

}
