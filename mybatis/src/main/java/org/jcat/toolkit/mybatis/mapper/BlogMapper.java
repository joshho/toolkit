package org.jcat.toolkit.mybatis.mapper;

import org.apache.ibatis.annotations.Select;
import org.jcat.toolkit.mybatis.po.Blog;

import java.util.List;

public interface BlogMapper {
	//@Select("SELECT * FROM blog WHERE id = #{id}")
	Blog selectBlog(int id);
	
	List<Blog> selectBlogList(String name);
	
	List<Blog> selectAuthorBlogList(String userId);
	
	void addBlog(Blog blog);
	
	void updateBlog(Blog blog);
	
	void deleteBlog(String id);

	void deleteAll();
}