package org.jcat.toolkit.mybatis.mapper;

import java.util.List;

import org.jcat.toolkit.mybatis.po.Author;

public interface AuthorMapper {

    void add(Author author);

	List<Author> listAuthor();

    void deleteAll();
	
}